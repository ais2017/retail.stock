import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ManagerComponent} from "./manager/manager.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  { path: 'manager', component: ManagerComponent },
  { path: '', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
