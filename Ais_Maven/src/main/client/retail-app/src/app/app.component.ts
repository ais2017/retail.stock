import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {until} from "selenium-webdriver";
import urlContains = until.urlContains;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  OnInit{


  nonLogin: boolean;

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    this.router.events.subscribe( event => {
      if (event instanceof NavigationEnd) {
        if (location.pathname != '/') {
          this.nonLogin = true;
        }
        else this.nonLogin = false;
      }
    })
  }


}
