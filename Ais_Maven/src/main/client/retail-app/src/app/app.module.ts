import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ManagerComponent} from "./manager/manager.component";
import {FooterComponent} from "./footer/footer.component";
import {HeaderComponent} from "./header/header.component";
import {LoginComponent} from "./login/login.component";
import {FixedRightComponent} from "./fixed_right/fixed.right.component";
import {UserService} from "./service/user.service";
import {CellContentComponent} from "./manager/cell/cell-content.component";
import {ManagerService} from "./service/manager.service";
import {Api2Service} from "./service/api2.service";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {OrderFormComponent} from "./order-form/order-form.component";

@NgModule({
  declarations: [
    AppComponent,
    ManagerComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    FixedRightComponent,
    CellContentComponent,
    OrderFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [UserService, ManagerService, Api2Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
