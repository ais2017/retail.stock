import {Component, OnInit} from "@angular/core";
import {User} from "../model/user";
import {UserService} from "../service/user.service";

@Component({
  selector: 'header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{

  user: User;

  constructor(public userService: UserService) {
    this.userService.fetchUser().subscribe( user => {
      this.user = user;
    })
  }

  ngOnInit(): void {

  }

}
