import {Component, Input, OnChanges, OnInit} from "@angular/core";
import {ManagerService} from "../../service/manager.service";
import {Cell} from "../../model/cells";
import {Good} from "../../model/good";

@Component({
  selector: 'cell-content',
  templateUrl: './cell-content.component.html',
  styleUrls: ['./cell-content.component.scss']
})
export class CellContentComponent implements OnInit, OnChanges{
  @Input() activeId: number;

  cells: Cell[];
  food: Good[];
  activeCell: number;
  constructor(private managerService: ManagerService) {
  }

  ngOnInit(): void {

  }

  ngOnChanges() {
    this.activeCell = null;
    this.managerService.getCellsByIdStoke(this.activeId).subscribe(cells => {
      this.cells = cells
    });
  }

  setActiveCell(id: number) {
    this.activeCell = id;
    this.managerService.getFoodByIdCell(this.activeCell).subscribe( food => {
      console.log(this.food);
      this.food = food
    });
  }

  clearCell() {
    this.activeCell = null;
  }
}
