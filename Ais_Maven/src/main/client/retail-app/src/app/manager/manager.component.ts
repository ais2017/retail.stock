import {Component, OnInit} from "@angular/core";
import {UserService} from "../service/user.service";
import {User} from "../model/user";
import {Stoke} from "../model/stoke";
import {ManagerService} from "../service/manager.service";

@Component({
  selector: 'manager-component',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss']
})
export class ManagerComponent implements OnInit{

  user: User;
  stocks: Stoke[];
  activeIdStoke: number;
  showOrder: boolean = false;

  constructor(private userService: UserService,
              private managerService: ManagerService) {
    this.userService.fetchUser().subscribe( user => {
      this.user = user;
    })
  }

  ngOnInit(): void {
    this.managerService.availableStock().subscribe( stocks => {
      this.stocks = stocks;
    });
  }

  setActiveStock(id: number): void {
    this.activeIdStoke = id;
  }

  onShowOrderForm() {
    this.showOrder = !this.showOrder;
  }
}
