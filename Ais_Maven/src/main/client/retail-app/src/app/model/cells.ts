import {Good} from "./good";

export class Cell {
  cellId: number;
  isCold: boolean;
  goods: Good[];
  height: number;
  length: number;
  width: number;
  index: number;

  constructor(params: any) {
    this.cellId = params.cellId;
    this.isCold = params.isCold;
    this.height = params.height;
    this.length = params.length;
    this.width = params.width;
    this.index = params.index;
  }
}
