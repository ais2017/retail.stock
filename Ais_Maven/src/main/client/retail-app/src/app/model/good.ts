export class Good {

  width: number;
  height: number;
  legth: number;
  weigth: number;
  cost: number;
  metric: string;
  status: string;
  importer: string;
  cellId: number;
  boxId: number;
  partId: number;
  amount: number;

  constructor(props) {
    this.width = props.width;
    this.height = props.height;
    this.legth = props.legth;
    this.weigth = props.weigth;
    this.cost = props.cost;
    this.metric = props.metric;
    this.status = props.status;
    this.importer = props.importer;
    this.cellId = props.cellId;
    this.boxId = props.boxId;
    this.partId = props.partId;
    this.amount = props.amount;
  }
}

