import {Cell} from "./cells";

export class Stoke {
  boxId: number;
  xCoordinate: string;
  yCoordinate: string;
  owner: string;
  cells: Cell[];

  constructor(params: any) {
    this.boxId = params.boxId;
    this.xCoordinate = params.xCoordinate;
    this.yCoordinate = params.yCoordinate;
    this.owner = params.owner;
  }
}
