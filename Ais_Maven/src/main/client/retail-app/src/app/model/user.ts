export class User {
  id: string;
  name: string;
  role: string;

  constructor(params: any) {
    this.id = params.id;
    this.name = params.name;
    this.role = params.role;
  }
}
