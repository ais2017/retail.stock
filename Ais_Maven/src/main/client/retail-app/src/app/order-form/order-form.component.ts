import {Component, Input, OnInit} from "@angular/core";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Stoke} from "../model/stoke";

@Component({
  selector: 'order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent implements OnInit{

  form: FormGroup = new FormGroup({});
  orderType: boolean = false;
  offersReturn = ['Списание', 'Возврат'];
  offersOrder = ['Заказ', 'Поставка'];

  @Input() stoke: number;

  selectOfferOrder: FormControl = new FormControl(this.offersOrder[0]);
  selectOfferReturn: FormControl = new FormControl(this.offersReturn[0]);

  constructor() {

  }

  ngOnInit(): void {
    let form = new FormControl(false);
    this.form.addControl(
      'orderType', form);
    this.form.addControl(
      'offerOrder', this.selectOfferOrder);
    this.form.addControl(
      'offerReturn', this.selectOfferReturn);

    form.valueChanges.subscribe( value => {
      value? this.selectOfferOrder.setValue(this.offersOrder[0]) :
        this.selectOfferReturn.setValue(this.offersReturn[0]);
      this.orderType = value;
    });
  }

}
