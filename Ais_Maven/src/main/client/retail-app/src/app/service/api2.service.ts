import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

/**
 Официальное руководство:
 https://angular.io/guide/http

 1) отправка объекта в виде URLPARAMS

 let httpParams = new HttpParams();
 Object.keys(attributes).forEach(function (key) {
	httpParams = httpParams.append(key, attributes[key]?attributes[key]:'');
 });
 this.api2.get('/cards-list', {params: httpParams});

 При переходе на 5 ангуляр можно использовать более простой способ.
 https://stackoverflow.com/questions/45210406/angular-4-3-httpclient-set-params

 2) чтобы вернуть текст, нужно передать в options: {responseType: 'text'}


 Примеры:

 create(): Observable<string> {
        return this.api2.post('/create-api-key', null, {responseType: 'text'});
 }

 delete(apiKey: string): Observable<{}> {
        return this.api2.delete(`/delete-api-key/${apiKey}`);
 }

 get(): Observable<ApiKeyData> {
        return this.api2.get('/get-api-key').map(resp => {
                if (!resp) {
                    return null;
                } else {
                    return resp;
                }
            }
        );
 }

 */
@Injectable()
export class Api2Service {
  private static API_NAME = 'api';
  public static API = Api2Service.API_NAME + '/';

  constructor(private http: HttpClient) {
  }

  get(url: string, options?: Object): Observable<any> {
    return this.http.get(Api2Service.API + url, options);
  }

  post(url: string, body: any, options?: Object): Observable<any> {
    return this.http.post(Api2Service.API + url, body, options);
  }

  put(url: string, body: any, options?: Object): Observable<any> {
    return this.http.put(Api2Service.API + url, body, options);
  }

  delete(url: string, options?: Object): Observable<any> {
    return this.http.delete(Api2Service.API + url, options);
  }

  patch(url: string, body: any, options?: Object): Observable<any> {
    return this.http.patch(Api2Service.API + url, body, options);
  }

  head(url: string, options?: Object): Observable<any> {
    return this.http.head(Api2Service.API + url, options);
  }

  options(url: string, options?: Object): Observable<any> {
    return this.http.options(Api2Service.API + url, options);
  }
}
