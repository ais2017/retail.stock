import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import { of } from 'rxjs';
import {Stoke} from "../model/stoke";
import {Good} from "../model/good";
import {Cell} from "../model/cells";
import {Api2Service} from "./api2.service";

@Injectable({
  providedIn: 'root',
})
export class ManagerService {

  constructor(private apiService: Api2Service) {

  }

  availableStock(): Observable<Stoke[]> {
    let stoks: Stoke[] = new Array();
    stoks.push((new Stoke({boxId: 2, xCoordinate: '3', yCoordinate: '4', owner: 'ООО Возим туды-сюды'})));
    stoks.push((new Stoke({boxId: 3, xCoordinate: '5', yCoordinate: '46', owner: 'ОАО Не лезь'})));
    return of(stoks);
  }

  getFoodByIdCell(id: number): Observable<Good[]> {
    let good: Good[] = new Array();
    if (id == 2) {
      good.push(new Good({
        width: 1, height: 2, legth: 3, weigth: 4, cost: 5, metric: 'rub', status: 'In Progress', importer: 'X-5Retail',
        cellId: 9, boxId: 10, partId: 11, amount: 12
      }));

      good.push(new Good({
        width: 2, height: 3, legth: 5, weigth: 4, cost: 100, metric: 'usd', status: 'Unknown', importer: 'ASHAN',
        cellId: 9, boxId: 10, partId: 11, amount: 12
      }));
    }
    else {
      good.push(new Good({
        width: 3, height: 5, legth: 16, weigth: 4, cost: 51, metric: 'UAN', status: 'Finished', importer: 'My_sklad',
        cellId: 9, boxId: 10, partId: 11, amount: 12
      }));

      good.push(new Good({
        width: 2, height: 3, legth: 5, weigth: 4, cost: 150, metric: 'EUR', status: 'Start', importer: 'LINIYA',
        cellId: 9, boxId: 10, partId: 11, amount: 12
      }));
    }
    return of(good);
  }

  getCellsByIdStoke(id: number): Observable<Cell[]> {
    let good: Cell[] = new Array();
    if (id == 2) {
      good.push(new Cell({
        cellId: 1, isCold: true, height: 100, length: 200, width: 300, index: 1
      }));

      good.push(new Cell({
        cellId: 2, isCold: false, height: 300, length: 150, width: 200, index: 2
      }));
    }
    else {
      good.push(new Cell({
        cellId: 3, isCold: true, height: 600, length: 250, width: 120, index: 3
      }));
    }
    return of(good);
  }
}
