import {Injectable} from "@angular/core";
import {User} from "../model/user";
import {Observable} from "rxjs";
import { of } from 'rxjs';
import {Api2Service} from "./api2.service";

@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(private apiService: Api2Service) {

  }

  fetchUser(): Observable<User> {
    return of(new User({id: 1, name: "Михаил", role: "Менеджер"}));
  }
}
