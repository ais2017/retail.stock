package Reprository;

import model.building.Stoke;
import org.springframework.data.repository.CrudRepository;


public interface StokeRepository extends CrudRepository<Stoke, Integer> {

}