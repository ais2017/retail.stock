package controller;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import service.abs.OrderService;

@Data
@Controller
@RequestMapping(value = "/student")
public class AnalyticController {

    @Autowired
    OrderService orderService;

    @Autowired
    Gson gson;

}
