package exceptionDir.model;

public class OverCellException extends Exception {

    String msg;

    public OverCellException(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return super.toString() + "Your cell is totally full";
    }
}
