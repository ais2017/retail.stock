package exceptionDir.model;

public class OverSizeException extends Exception {

    String msg;

    public OverSizeException(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return super.toString() + "Your cell is size limit up";
    }
}