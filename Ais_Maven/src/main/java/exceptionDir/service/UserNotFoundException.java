package exceptionDir.service;

public class UserNotFoundException  extends Exception {

    String msg;

    public UserNotFoundException(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return super.toString() + "User with name " + msg + "is not found";
    }
}
