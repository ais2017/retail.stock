package model.building;

import model.goods.Good;
import model.stats.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

abstract public class Box {

    private int boxId;
    private int xCoordinate;
    private int yCoordinate;

    private String owner;
    private List<Cell> cells;

    public Box(int boxId, int xCoordinate, int yCoordinate) {
        this.boxId = boxId;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        cells = new ArrayList<>();
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    abstract Good getBestGood(String type);

    @Override
    public String toString() {
        return "Box{" +
                "boxId=" + boxId +
                ", xCoordinate=" + xCoordinate +
                ", yCoordinate=" + yCoordinate +
                ", owner='" + owner + '\'' +
                ", cells=" + cells +
                '}';
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    abstract public void publishDocs(Document document);

    abstract public Document getDocumentPeriod(Date date);

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }

    public void addCell(Cell cell) {
        cells.add(cell);
    }
}
