package model.building;

import exceptionDir.model.OverSizeException;
import model.goods.Food;
import model.goods.Good;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Cell {

    private int cellId;
    private boolean isCold;
    private List<Good> goods;
    private int height;
    private int length;
    private int width;
    private int index;

    public Cell(int cellId, boolean isCold, int height, int length, int width) {
        this.cellId = cellId;
        this.isCold = isCold;
        this.height = height;
        this.length = length;
        this.width = width;
        goods = new ArrayList<>();
    }


    private int boxId;

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public boolean isCold() {
        return isCold;
    }

    public void setCold(boolean cold) {
        isCold = cold;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "Cell{" +
                "cellId=" + cellId +
                ", isCold=" + isCold +
                ", goods=" + goods +
                ", height=" + height +
                ", length=" + length +
                ", width=" + width +
                ", boxId=" + boxId +
                '}';
    }

    public Food getBestFood(String type) {
        return goods.stream()
                .filter(good -> {
                    return good instanceof Food;
                }).map(good -> (Food)good)
                .filter(food -> food.getStatus().equals(type))
                .min( Comparator.comparingInt(Food::getHoursLeft)).get();
    }

    public List<Food> getCherishedFood() {
        return goods.stream()
                .filter(good -> {
                    return good instanceof Food;
                }).map(good -> (Food)good)
                .filter(food -> food.getHoursLeft() <= 0).collect(Collectors.toList());
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public void putGood(Good good) throws OverSizeException {
        if (good.getHeight() > this.getHeight() || good.getLegth() > this.getLength() || good.getWidth() > this.getWidth())
            throw new OverSizeException("cell overflow");
        good.setCellId(index);
        goods.add(good);
        recalc();
    }

    private void recalc() {
        double value = 1;
        for (Good good : goods) {
           value *=  1.41 * good.getWidth() * good.getLegth() * good.getHeight();
        }
        double koef = 1 - (value / (this.getWidth() * this.getLength() * this.getHeight()));
        int newValue = (int)(this.getHeight() * koef);
        this.setHeight(newValue);
        newValue = (int)(this.getWidth() * koef);
        this.setWidth(newValue);
        newValue = (int)(this.getLength() * koef);
        this.setLength(newValue);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
