package model.building;

import exceptionDir.model.OverCellException;
import exceptionDir.model.OverSizeException;
import model.goods.Food;
import model.goods.Good;
import model.goods.Partition;
import model.goods.Thing;
import model.stats.Document;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;


public class Stoke extends Box {

    public Stoke(int boxId, int xCoordinate, int yCoordinate) {
        super(boxId, xCoordinate, yCoordinate);
    }

    @Override
    Good getBestGood(String type) {
        return  getCells().stream().map( cell -> cell.getBestFood(type))
                .min( Comparator.comparingInt(Food::getHoursLeft)).get();
    }

    public void place(Partition part) throws OverCellException {
        while (part.getFoods().entrySet().size() != 0) {
            for (Cell cell : getCells()) {
                for (Map.Entry<Good, Integer> integerEntry : part.getFoods().entrySet()) {
                    try {
                        for (int i = integerEntry.getValue(); i > 0; i--) {
                            cell.putGood(integerEntry.getKey());
                            integerEntry.setValue(integerEntry.getValue() - 1);
                        }
                    } catch (OverSizeException ex) {
                        if (cell == getCells().get(getCells().size() - 1))
                            throw new OverCellException("Cell is over");
                        continue;
                    }
                }
            }
            part.getFoods().entrySet().removeIf( set -> {
                return set.getValue().equals(0);
            });
        }
    }

    @Override
    public String toString() {
        return "Stoke{}" + super.toString();
    }

    public List<Good> upload(Partition part) {
        List<Good> goods = new ArrayList<>();
        for (Map.Entry<Good, Integer> entry : part.getFoods().entrySet()) {
            for (Integer i = 0; i < entry.getValue(); i++) {
                Good good = getBestGood(entry.getKey().getStatus());
                goods.add(good);
                getCells().get(good.getCellId()).getGoods().remove(good);
            }
        }
        return goods;
    }

    @Override
    public void publishDocs(Document document) {

    }

    public List<Food> isGoodDie() {
        ArrayList<Food> foods = new ArrayList<>();
        List<List<Food>> lists =  getCells().stream().map( cell -> cell.getCherishedFood()).collect(Collectors.toList());
        lists.forEach(list -> {
            foods.addAll(list);
        });
        return foods;
    }

    @Override
    public Document getDocumentPeriod(Date date) {
        return null;
    }

    public Map<Cell, List<Integer>> cherishedFood() {
        Map<Cell,List<Integer>> map;
        for (Cell cell : getCells()) {
            for (Good good : cell.getGoods()) {
                List<Integer> ids = new ArrayList<>();
                Food tempFood;
                Thing tempThing;
                if (good instanceof Food) {
                    tempFood = (Food) good;
                }
                if (good instanceof Thing) {
                    tempThing = (Thing) good;
                }
            }
        }
        return null;
    }

    public void setIndex() {
        for (int i = 0; i < getCells().size(); i++) {
            getCells().get(i).setIndex(i);
        }
    }
}
