package model.events;

public class Event {

    public Event() {}

    private int eventId;
    private int objectId;
    private String type;
    private String message;

    @Override
    public String toString() {
        return "Event{" +
                "eventId=" + eventId +
                ", objectId=" + objectId +
                ", type='" + type + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
