package model.goods;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Food extends Good implements ShelfLifer, Cherished {

    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");

    private Date dateOfManufacture;

    private Date shelftDate;

    public Food(int width, int height, int legth, int weight, int cost, String metric, String status, Date dateOfManufacture, Date shelftDate) {
        super(width, height, legth, weight, cost, metric, status);
        this.dateOfManufacture = dateOfManufacture;
        this.shelftDate = shelftDate;
    }

    @Override
    public String toString() {
        return "Food{" +
                "ft=" + ft +
                ", dateOfManufacture=" + dateOfManufacture +
                ", shelftDate=" + shelftDate +
                '}';
    }

    public int getHoursLeft() {
        long diff;
        int hours = 0;
        if (shelftDate != null) {
            diff = shelftDate.getTime() - new Date().getTime();
            hours = (int)(diff / (60 * 60 * 1000));
        }
        return hours;
    }

    public Date getShelfTime() {
        return shelftDate;
    }

    public void publishAlert() {

    }

    public void publishDestroyInfo() {

    }

    public void setShelftDate(Date shelftDate) {
        this.shelftDate = shelftDate;
    }

    public void setDateOfManufacture(Date dateOfManufacture) {
        this.dateOfManufacture = dateOfManufacture;
    }

    public Date getDateOfManufacture() {
        return dateOfManufacture;
    }
}
