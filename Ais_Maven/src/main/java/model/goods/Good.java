package model.goods;

abstract public class Good {

    private int width;
    private int height;
    private int legth;
    private int weight;
    private int cost;
    private String metric;
    private String status;

    private String importer;
    private int cellId;
    private int boxId;
    private int partId;
    private int amount;

    public Good(int width, int height, int legth, int weight, int cost, String metric, String status) {
        this.width = width;
        this.height = height;
        this.legth = legth;
        this.weight = weight;
        this.cost = cost;
        this.metric = metric;
        this.status = status;
    }

    public int getVolume() {
        return this.height * this.width * this.legth;
    }

    public int getSquare() {
        return this.width * this.legth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLegth() {
        return legth;
    }

    public void setLegth(int legth) {
        this.legth = legth;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getImporter() {
        return importer;
    }

    public void setImporter(String importer) {
        this.importer = importer;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Good{" +
                "width=" + width +
                ", height=" + height +
                ", legth=" + legth +
                ", weight=" + weight +
                ", cost=" + cost +
                ", metric='" + metric + '\'' +
                ", status='" + status + '\'' +
                ", importer='" + importer + '\'' +
                ", cellId=" + cellId +
                ", boxId=" + boxId +
                ", partId=" + partId +
                ", amount=" + amount +
                '}';
    }
}
