package model.goods;

import java.util.Map;

public class Partition {

    private Map<Good, Integer> foods;

    private String partitionNotes;

    private int idPart;

    public Partition(int idPart, Map<Good, Integer> foods) {
        this.idPart = idPart;
        this.foods = foods;
    }

    public int getMinShelfHours() {
        return foods.entrySet().stream().map(Map.Entry::getKey)
                .filter(good -> {
                return good instanceof Food;
            }).map(good -> {
                return ((Food) good).getHoursLeft();
            }).min(Integer::compareTo).get();

    }

    public int getIdPart() {
        return idPart;
    }

    @Override
    public String toString() {
        return "Partition{" +
                "foods=" + foods +
                ", partitionNotes='" + partitionNotes + '\'' +
                ", idPart=" + idPart +
                '}';
    }

    public void setIdPart(int idPart) {
        this.idPart = idPart;
    }

    public String getPartitionNotes() {
        return partitionNotes;
    }

    public void setPartitionNotes(String partitionNotes) {
        this.partitionNotes = partitionNotes;
    }

    public Map<Good, Integer> getFoods() {
        return foods;
    }

    public void setFoods(Map<Good, Integer> foods) {
        this.foods = foods;
    }

    //СЕКЦИЯ В МОДЕЛЬ
    public void addToPart(Good good, Integer amount) {
        foods.put(good,amount);
    }
}
