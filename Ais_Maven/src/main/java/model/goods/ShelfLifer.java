package model.goods;

import java.util.Date;

public interface ShelfLifer {

    int getHoursLeft();

    Date getShelfTime();

    void publishAlert();
}
