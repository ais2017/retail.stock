package model.goods;

public class Thing extends Good implements Cherished {

    public Thing(int width, int height, int legth, int weight, int cost, String metric, String status) {
        super(width, height, legth, weight, cost, metric, status);
    }

    @Override
    public String toString() {
        return "Thing{}";
    }

    public void publishDestroyInfo() {

    }
}
