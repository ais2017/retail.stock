package model.order;

import model.goods.Partition;

import java.util.Date;

abstract public class Order {

    private int id;

    private Partition partition;

    private Date registrationDate;

    private String status;

    private int managerId;

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private boolean exec;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", partition=" + partition +
                ", registrationDate=" + registrationDate +
                ", status='" + status + '\'' +
                ", managerId=" + managerId +
                ", type='" + type + '\'' +
                ", exec=" + exec +
                '}';
    }

    public Order(int id, Partition partition, Date registrationDate, String status, String  type) {
        this.id = id;
        this.partition = partition;
        this.registrationDate = registrationDate;
        this.status = status;
        this.type = type;
    }

    public Order(Order order) {
        this.id = order.id;
        this.partition = order.partition;
        this.registrationDate = order.registrationDate;
        this.status = order.status;
        this.managerId = order.managerId;
        this.type = order.type;
        this.exec = order.exec;
    }

    public void setExecute(boolean exec) {
        this.exec = exec;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public Partition getPartition() {
        return partition;
    }

    public void setPartition(Partition partition) {
        this.partition = partition;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isExec() {
        return exec;
    }

    public void setExec(boolean exec) {
        this.exec = exec;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
