package model.order;

import com.sun.org.apache.xpath.internal.operations.Or;
import model.building.Box;
import model.goods.Partition;
import model.retailer.Shop;

import java.util.Date;

public class OrdinalOrder extends Order implements Periodically {

    private int periodDays;
    private boolean isPeriodical;
    private boolean isOrder;
    private Box placeFrom;
    private Box placeTo;
    private Date dateArrivedAllowed;
    private static String type = "ordinal";

    private Date dateArrived;
    private int orderChain;
    private boolean isWaiting;

    public OrdinalOrder(int id, Partition partition, Date registrationDate, String status, int periodDays, boolean isPeriodical,
                        boolean isOrder, Box placeFrom, Box placeTo, Date dateArrivedAllowed) {
        super(id, partition, registrationDate, status, type);
        this.periodDays = periodDays;
        this.isPeriodical = isPeriodical;
        this.isOrder = isOrder;
        this.placeFrom = placeFrom;
        this.placeTo = placeTo;
        this.dateArrivedAllowed = dateArrivedAllowed;
    }

    public OrdinalOrder(Order order) {
        super(order);
    }

    public boolean isNextPeriodSoon() {
        return false;
    }

    public boolean isNextPeriodComing() {
        return false;
    }

    public boolean isPeriodical() {
        return isPeriodical;
    }

    public void setPeriodical(boolean periodical) {
        isPeriodical = periodical;
    }

    public int getPeriodDays() {
        return periodDays;
    }

    public void setPeriodDays(int periodDays) {
        this.periodDays = periodDays;
    }

    public int getOrderChain() {
        return orderChain;
    }

    public void setOrderChain(int orderChain) {
        this.orderChain = orderChain;
    }

    public Box getPlaceFrom() {
        return placeFrom;
    }

    public void setPlaceFrom(Box placeFrom) {
        this.placeFrom = placeFrom;
    }

    public Box getPlaceTo() {
        return placeTo;
    }

    public void setPlaceTo(Box placeTo) {
        this.placeTo = placeTo;
    }

    public Date getDateArrived() {
        return dateArrived;
    }

    public void setDateArrived(Date dateArrived) {
        this.dateArrived = dateArrived;
    }

    public Date getDateArrivedAllowed() {
        return dateArrivedAllowed;
    }

    public void setDateArrivedAllowed(Date dateArrivedAllowed) {
        this.dateArrivedAllowed = dateArrivedAllowed;
    }

    public boolean isWaiting() {
        return isWaiting;
    }

    public void setWaiting(boolean waiting) {
        isWaiting = waiting;
    }

    public boolean isOrder() {
        return isOrder;
    }

    public void setOrder(boolean order) {
        isOrder = order;
    }
}
