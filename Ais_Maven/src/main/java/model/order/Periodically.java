package model.order;

public interface Periodically {

    public boolean isNextPeriodSoon();

    public boolean isNextPeriodComing();
}
