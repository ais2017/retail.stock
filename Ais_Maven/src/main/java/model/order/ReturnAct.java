package model.order;

import model.goods.Partition;

import java.util.Date;

public class ReturnAct extends Order {

    private int shopId;

    private boolean isCancel;
    private String reason;
    private String cost;

    private static String type = "return";


    public ReturnAct(int id, Partition partition, Date registrationDate, String status, boolean isCancel) {
        super(id, partition, registrationDate, status, type);
        this.isCancel = isCancel;
    }

    public ReturnAct(Order order) {
        super(order);
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public boolean isCancel() {
        return isCancel;
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
