package model.person;

public class Admin extends User {

    public Admin(int userId, String name) {
        super(userId, name);
    }

    public void setRoles(String role) {

    }

    public void addUser(User user) {

    }

    public void deleteUser(User user) {

    }

    @Override
    void authorize() {

    }

    @Override
    void getHistory() {

    }

    @Override
    void saveHistory() {

    }

    public void setUserParameters() {

    }

    public void setAvaliableConts() {

    }

    @Override
    public String toString() {
        return "Admin{}";
    }
}
