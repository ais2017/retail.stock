package model.person;

import model.building.Box;
import model.retailer.Shop;
import model.stats.BoxAnalize;
import model.stats.PeriodAnalyse;
import model.stats.ReportAnalize;
import model.stats.ShopAnalize;

import java.util.List;

public class Analyst extends User {

    private List<ShopAnalize> shopAnalizes;
    private List<PeriodAnalyse> periodAnalyses;
    private List<BoxAnalize> boxAnalizes;
    private List<Box> boxList;

    public Analyst(int userId, String name) {
        super(userId, name);
    }

    public ReportAnalize analizePeriod(List<PeriodAnalyse> periodAnalyses) {
        return null;
    }

    public ReportAnalize analizeShop(List<ShopAnalize> shopAnalizes) {
        return null;
    }

    @Override
    public String toString() {
        return "Analyst{" +
                "shopAnalizes=" + shopAnalizes +
                ", periodAnalyses=" + periodAnalyses +
                ", boxAnalizes=" + boxAnalizes +
                ", boxList=" + boxList +
                '}';
    }

    public ReportAnalize analizeBox(List<BoxAnalize> shopAnalizes) {
        return null;
    }



    public void setAsBlack(Shop shop) {

    }

    public void publishMsg(String msg) {

    }

    public List<Box> getBoxList() {
        return null;
    }

    @Override
    void authorize() {

    }

    @Override
    void getHistory() {

    }

    @Override
    void saveHistory() {

    }

    public List<BoxAnalize> getBoxAnalizes() {
        return boxAnalizes;
    }

    public void setBoxAnalizes(List<BoxAnalize> boxAnalizes) {
        this.boxAnalizes = boxAnalizes;
    }

    public List<PeriodAnalyse> getPeriodAnalyses() {
        return periodAnalyses;
    }

    public void setPeriodAnalyses(List<PeriodAnalyse> periodAnalyses) {
        this.periodAnalyses = periodAnalyses;
    }

    public List<ShopAnalize> getShopAnalizes() {
        return shopAnalizes;
    }

    public void setShopAnalizes(List<ShopAnalize> shopAnalizes) {
        this.shopAnalizes = shopAnalizes;
    }

    public void setBoxList(List<Box> boxList) {
        this.boxList = boxList;
    }
}
