package model.person;

import model.building.Box;
import model.building.Cell;
import model.building.Stoke;
import model.goods.Good;
import model.order.Order;
import model.order.OrdinalOrder;
import model.order.ReturnAct;
import model.retailer.Shop;
import service.DummyOrderService;

import java.util.List;

public class Manager extends User {

    private boolean isShopManager;
    private DummyOrderService dummyOrderService = new DummyOrderService();

    public Manager(int userId, String name, boolean isShopManager) {
        super(userId, name);
        this.isShopManager = isShopManager;
    }

    public void createOrdinalOrder(OrdinalOrder ordinalOrder) {
        dummyOrderService.putOrder(ordinalOrder);
    }

    @Override
    public String toString() {
        return "Manager{" +
                "isShopManager=" + isShopManager +
                ", dummyOrderService=" + dummyOrderService +
                '}';
    }

    public void createReturnAct(ReturnAct returnAct) {
        dummyOrderService.putOrder(returnAct);
    }

    public Stoke createStoke(Stoke stoke) {
        return stoke;
    }

    public Shop createShop(Shop shop) {
        return shop;
    }

    public void addToInvoice(Order order, Good good, Integer amount) {
        order.getPartition().addToPart(good, order.getPartition().getFoods().get(good) + amount);
    }

    public void expansionStoke(Stoke stoke, Cell cell) {
        stoke.getCells().add(cell);
    }

    public void changeStatus(Order order, String status) {
        order.setStatus(status);
    }

    public void publishImporter(Shop shop, String act) {

    }

    public void publishBox(Box box, String act) {

    }

    public List<Order> getAllOrder() {
        return null;
    }

    @Override
    void authorize() {

    }

    @Override
    void getHistory() {

    }

    @Override
    void saveHistory() {

    }
}
