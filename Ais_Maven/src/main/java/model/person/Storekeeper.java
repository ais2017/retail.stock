package model.person;

import exceptionDir.model.OverCellException;
import model.building.Stoke;
import model.goods.Partition;
import model.order.Order;

import java.util.List;

public class Storekeeper extends User {

    private int boxId;
    private List<Order> documents;

    @Override
    public String toString() {
        return "Storekeeper{" +
                "boxId=" + boxId +
                ", documents=" + documents +
                '}';
    }

    public Storekeeper(int userId, String name) {
        super(userId, name);
    }

    public boolean putGood(Stoke stoke, Partition partition) {
        try {
            stoke.place(partition);
        } catch (OverCellException e) {
            return false;
        }
        return true;
    }

    public boolean removeGood(Stoke stoke, Partition partition) {
        stoke.upload(partition);
        return true;
    }

    public void changeStatus(Order order, String status) {
        order.setStatus(status);
    }

    public List<Order> getAllowedDocs() {
        return this.documents;
    }

    public void setDocumentAsExec(Order order, boolean flag) {
        order.setExec(flag);
    }

    public void publishMessage(String msg) {

    }

    @Override
    void authorize() {

    }

    @Override
    void getHistory() {

    }

    @Override
    void saveHistory() {

    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public void setBusy (List<Integer> cellId) {

    }

    public void setFree (List<Integer> cellId) {

    }

    public void sendNote (int idPart) {

    }

    public List<Order> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Order> documents) {
        this.documents = documents;
    }
}
