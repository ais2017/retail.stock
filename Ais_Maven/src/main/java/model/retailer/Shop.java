package model.retailer;

import model.building.Stoke;
import model.person.Manager;

public class Shop {

    private int shopId;
    private boolean isImporter;
    private String info;
    private Manager manager;
    private Stoke stoke;
    private boolean isBanned;

    public Shop(int shopId, boolean isImporter, Manager manager, Stoke stoke) {
        this.shopId = shopId;
        this.isImporter = isImporter;
        this.manager = manager;
        this.stoke = stoke;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isImporter() {
        return isImporter;
    }

    public void setImporter(boolean importer) {
        isImporter = importer;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Stoke getStoke() {
        return stoke;
    }

    public void setStoke(Stoke stoke) {
        this.stoke = stoke;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "shopId=" + shopId +
                ", isImporter=" + isImporter +
                ", info='" + info + '\'' +
                ", manager=" + manager +
                ", stoke=" + stoke +
                '}';
    }

    public boolean isBanned() {
        return isBanned;
    }

    public void setBanned(boolean banned) {
        isBanned = banned;
    }
}
