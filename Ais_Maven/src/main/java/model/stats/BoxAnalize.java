package model.stats;

import model.building.Box;

public class BoxAnalize extends Document {

    private int boxId;

    private static String type = "box";

    public BoxAnalize(int docId) {
        super(docId, type);
    }

    public BoxAnalize(Document document) {
        super(document);
    }

    @Override
    public String toString() {
        return "BoxAnalize{" +
                "boxId=" + boxId +
                '}';
    }

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }
}
