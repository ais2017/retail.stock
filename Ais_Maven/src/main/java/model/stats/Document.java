package model.stats;

abstract public class Document {

    private int docId;
    private String type;
    private String name;
    private String info;

    public Document(int docId, String type) {
        this.docId = docId;
        this.type = type;
    }

    public Document(Document document) {
        this.setDocId(document.getDocId());
        this.setInfo(document.getInfo());
        this.setName(document.getName());
        this.setType(document.getType());
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }
}
