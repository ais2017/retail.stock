package model.stats;

public class PeriodAnalyse extends Document {

    private static String type = "period";

    public PeriodAnalyse(int docId) {
        super(docId, type);
    }

    public PeriodAnalyse(Document document) {
        super(document);
    }

    @Override
    public String toString() {
        return "PeriodAnalyse{}";
    }
}
