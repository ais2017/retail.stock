package model.stats;

public class ReportAnalize extends Document {

    private String result;

    private static String type = "report";

    public ReportAnalize(int docId) {
        super(docId, type);
    }

    public ReportAnalize(Document document) {
        super(document);
    }

    public void generateDoc() {}

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ReportAnalize{" +
                "result='" + result + '\'' +
                '}';
    }

    public void setResult(String result) {
        this.result = result;
    }
}
