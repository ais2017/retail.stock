package model.stats;


public class ShopAnalize extends Document {

    private int shopId;

    private static String type = "shop";

    public ShopAnalize(int docId) {
        super(docId, type);
    }

    public ShopAnalize(Document document) {
        super(document);
    }

    public int getShopId() {
        return shopId;
    }

    @Override
    public String toString() {
        return "ShopAnalize{" +
                "shopId=" + shopId +
                '}';
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }
}
