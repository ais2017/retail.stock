package service;

import model.stats.*;
import service.abs.DocumentService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DummyDocumentService extends DocumentService {

    public static List<Document> documents;

    public DummyDocumentService() {
        documents = new ArrayList<>();
        Document document = new PeriodAnalyse(1);
        Document document1 = new BoxAnalize(1);
        ((BoxAnalize) document1).setBoxId(2);
        document1.setName("BoxInMonth");
        document1.setInfo("Box is bad");
        document.setInfo("Year-Period");
        document.setName("Good Period");
    }

    @Override
    public void putDocument(Document document) {
        documents.removeIf(document1 -> document1.getDocId() ==
                document.getDocId());
        documents.add(document);
    }

    @Override
    public Document getDocumentById(int id) {
        for (Document document : documents) {
            if (document.getDocId() == id)
                return cloneDoc(document);
        }
        return null;
    }

    @Override
    public List<Document> getDocumentByType(String type) {
        List<Document> resultArr = new ArrayList<>(documents);
        resultArr.removeIf( document -> !document.getType().equals(type));
        return resultArr;
    }

    @Override
    public List<Document> getAllDocument() {
        return new ArrayList<>(documents);
    }

    private Document cloneDoc(Document document) {
        if (document.getType().equals("box")) {
            BoxAnalize documentClone = new BoxAnalize(document);
            BoxAnalize originalBox = (BoxAnalize)document;
            documentClone.setBoxId(originalBox.getBoxId());
            Document docClone = documentClone;
            return docClone;
        }
        if (document.getType().equals("period")) {
            PeriodAnalyse documentClone = new PeriodAnalyse(document);
            PeriodAnalyse originalBox = (PeriodAnalyse) document;
            Document docClone = documentClone;
            return docClone;
        }
        if (document.getType().equals("report")) {
            ReportAnalize documentClone = new ReportAnalize(document);
            ReportAnalize originalBox = (ReportAnalize) document;
            documentClone.setResult(originalBox.getResult());
            Document docClone = documentClone;
            return docClone;
        }
        if (document.getType().equals("shop")) {
            ShopAnalize documentClone = new ShopAnalize(document);
            ShopAnalize originalBox = (ShopAnalize) document;
            Document docClone = documentClone;
            return docClone;
        }
        return null;
    }
}
