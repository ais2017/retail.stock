package service;

import model.events.Event;
import service.abs.HistoryService;

import java.util.ArrayList;
import java.util.List;

public class DummyHistoryService extends HistoryService {

    public static List<Event> events;

    public DummyHistoryService() {
        events = new ArrayList<>();
        Event event = new Event();
        event.setType("shop");
        event.setObjectId(2);
        event.setEventId(1);
        event.setMessage("Bad Shop");
    }

    @Override
    public void putEvent(Event event) {
        events.removeIf(event1 -> event1.getEventId() ==
                event.getEventId());
        events.add(event);
    }

    @Override
    public Event getEventById(int id) {
        for (Event event : events) {
            if (event.getEventId() == id)
                return cloneEvent(event);
        }
        return null;
    }

    @Override
    public List<Event> getEventsByObject(int id) {
        List<Event> events1 = new ArrayList<>(this.events);
        events1.removeIf(event -> event.getObjectId() != id);
        return events1;
    }

    @Override
    public List<Event> getAllEvents() {
        return new ArrayList<>(this.events);
    }

    private Event cloneEvent(Event event) {
        Event event1 = new Event();
        event1.setEventId(event.getEventId());
        event1.setMessage(event.getMessage());
        event1.setObjectId(event.getObjectId());
        event1.setType(event.getType());
        return event1;
    }
}
