package service;

import com.sun.org.apache.xpath.internal.operations.Or;
import model.building.Stoke;
import model.goods.Food;
import model.goods.Good;
import model.goods.Partition;
import model.order.Order;
import model.order.OrdinalOrder;
import model.order.ReturnAct;
import service.abs.OrderService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DummyOrderService extends OrderService {

    public static List<Order> orders;
    public DummyStokeService dummyStokeService;

    public DummyOrderService() {
        dummyStokeService = new DummyStokeService();
        orders = new ArrayList<>();
        Partition partition = new Partition(1, new HashMap<Good, Integer>());
        Food badFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 4*60*60*1000 - 1000));
        Food goodFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 5*24*60*60*1000 - 1000));
        partition.getFoods().put(badFood,5);
        partition.getFoods().put(goodFood,6);
        Order order = new OrdinalOrder(1, partition, new Date(), "start", 2, true, true,
                dummyStokeService.getStokeById(1), dummyStokeService.getStokeById(2),new Date());
        Order order2 = new OrdinalOrder(2, new Partition(2, new HashMap<Good, Integer>()), new Date(), "inProcess", 2, true, true,
                dummyStokeService.getStokeById(3), dummyStokeService.getStokeById(1),new Date());
        Order order3 = new ReturnAct(3, new Partition(1, new HashMap<Good, Integer>()), new Date(), "inProcess", false);
        Order order4 = new ReturnAct(4, partition, new Date(), "finish", true);
        orders.add(order);
        orders.add(order2);
        orders.add(order3);
        orders.add(order4);
    }

    @Override
    public void putOrder(Order order) {
        orders.removeIf(order1 -> order1.getId() ==
                order.getId());
        orders.add(order);
    }

    @Override
    public Order getOrderById(int id) {
        for (Order order : orders) {
            if (order.getId() == id)
                return cloneOrd(order);
        }
        return null;
    }

    @Override
    public List<Order> getOrdersByType(String type) {
        List<Order> orders = new ArrayList<>(this.orders);
        orders.removeIf(order -> !order.getType().equals(type));
        return orders;
    }

    @Override
    public List<Order> getAllOrders() {
        return new ArrayList<>(this.orders);
    }

    private Order cloneOrd(Order order) {
        if (order.getType().equals("ordinal")) {
            OrdinalOrder ordinalOrder = new OrdinalOrder(order);
            OrdinalOrder originalOrder = (OrdinalOrder) order;
            ordinalOrder.setDateArrived(originalOrder.getDateArrived());
            ordinalOrder.setDateArrivedAllowed(originalOrder.getDateArrivedAllowed());
            ordinalOrder.setOrder(originalOrder.isOrder());
            ordinalOrder.setOrderChain(originalOrder.getOrderChain());
            ordinalOrder.setPeriodDays(originalOrder.getPeriodDays());
            ordinalOrder.setPeriodical(originalOrder.isPeriodical());
            ordinalOrder.setPlaceFrom(originalOrder.getPlaceFrom());
            ordinalOrder.setPlaceTo(originalOrder.getPlaceTo());
            ordinalOrder.setWaiting(originalOrder.isWaiting());
            Order ordClone = ordinalOrder;
            return ordClone;
        }
        if (order.getType().equals("return")) {
            ReturnAct returnAct = new ReturnAct(order);
            ReturnAct originalAct = (ReturnAct) order;
            returnAct.setCancel(originalAct.isCancel());
            returnAct.setReason(originalAct.getReason());
            returnAct.setShopId(originalAct.getShopId());
            Order ordClone = returnAct;
            return ordClone;
        }
        return null;
    }
}
