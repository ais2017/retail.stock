package service;

import exceptionDir.model.OverCellException;
import model.building.Cell;
import model.building.Stoke;
import model.goods.Food;
import model.goods.Good;
import model.goods.Partition;
import service.abs.StokeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DummyStokeService extends StokeService {

    public static List<Stoke> stoks;

    public DummyStokeService() {
        stoks = new ArrayList<>();
        Stoke stoke = new Stoke(1, 1, 2);
        stoke.getCells().add(new Cell(0, true,100,100,100));
        stoke.getCells().add(new Cell(1, true,100,100,100));
        Stoke stoke2 = new Stoke(2, 2, 4);
        stoke2.getCells().add(new Cell(2, false,120,100,150));
        stoke2.getCells().add(new Cell(3, true,100,1050,105));
        Stoke stoke3 = new Stoke(3, 23, 43);
        stoke3.getCells().add(new Cell(4, true,120,100,150));
        Partition partition = new Partition(2, new HashMap<Good, Integer>());
        Food badFood = new Food(1,1,1,1,1,"kg", "income",  new Date(new Date().getTime() - 8*60*60*1000 - 1000), new Date(new Date().getTime() - 4*60*60*1000 - 1000));
        partition.addToPart(badFood,43);
        Partition partition2 = new Partition(2, new HashMap<Good, Integer>());
        Food goodFood = new Food(1,1,1,1,1,"kg", "income",  new Date(new Date().getTime() + 4*60*60*1000 - 1000), new Date(new Date().getTime() + 8*60*60*1000 - 1000));
        partition2.addToPart(goodFood,4);
        try {
            stoke2.place(partition);
        } catch (OverCellException e) {
            e.printStackTrace();
        }
        try {
            stoke3.place(partition2);
        } catch (OverCellException e) {
            e.printStackTrace();
        }
        stoke.setIndex();
        stoke2.setIndex();
        stoke3.setIndex();
        stoks.add(stoke);
        stoks.add(stoke2);
        stoks.add(stoke3);
    }

    @Override
    public void putStoke(Stoke stoke) {
        stoks.removeIf(stoke1 -> stoke1.getBoxId() == stoke.getBoxId());
        stoks.add(stoke);
    }

    @Override
    public Stoke getStokeById(int id) {
        for (Stoke stoke : stoks) {
            if (stoke.getBoxId() == id)
                return stoke;
        }
        return null;
    }

    @Override
    public List<Stoke> getAllStoks() {
        return stoks;
    }
}
