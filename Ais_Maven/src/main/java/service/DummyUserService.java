package service;

import exceptionDir.service.UserNotFoundException;
import model.person.*;
import service.abs.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DummyUserService extends UserService {

    private static List<User> users;
    private static List<User> currentUsers;
    private static Map<String, String> storePassword;

    public DummyUserService() {
        storePassword = new HashMap<>();
        storePassword.put("user-storekeeper", "1");
        storePassword.put("user-admin", "2");
        storePassword.put("user-analyst", "3");
        storePassword.put("user-manager", "4");
        users = new ArrayList<>();
        currentUsers = new ArrayList<>();
        Storekeeper storekeeper = new Storekeeper(1, "Bob");
        storekeeper.setUsername("user-storekeeper");
        storekeeper.setBoxId(2);
        storekeeper.setRole("storekeeper");
        users.add(storekeeper);
        Admin admin = new Admin(2, "Alice");
        admin.setUsername("user-admin");
        admin.setRole("admin");
        users.add(admin);
        Analyst analyst = new Analyst(3, "Marry");
        analyst.setUsername("user-analyst");
        analyst.setRole("analyst");
        users.add(analyst);
        Manager manager = new Manager(4, "Quentin", false);
        manager.setUsername("user-manager");
        manager.setRole("manager");
        users.add(manager);
    }

    @Override
    public User login(String username, String password) throws UserNotFoundException {
        if (!checkPassword(username, password))
            throw new UserNotFoundException(username);
        else {
            for (User user : users) {
                if (user.getUsername().equals(username))
                    return user;
            }
        }
        return null;
    }

    @Override
    public boolean checkPassword(String username, String password) {
        return (storePassword.containsKey(username) &&
                storePassword.get(username).equals(password));
    }

    @Override
    public boolean isUserLogined(User user) {
        return currentUsers.contains(user);
    }

    @Override
    public void logout(User user) {
        currentUsers.remove(user);
    }

    public User getUser(String role) {
        return users.stream().filter(user -> user.getRole().equals(role)).collect(Collectors.toList()).get(0);
    }
}
