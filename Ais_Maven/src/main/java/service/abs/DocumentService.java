package service.abs;

import model.stats.Document;

import java.util.List;

public abstract class DocumentService {

    public abstract void putDocument(Document document);

    public abstract Document getDocumentById(int id);

    public abstract List<Document> getDocumentByType(String type);

    public abstract List<Document> getAllDocument();
}
