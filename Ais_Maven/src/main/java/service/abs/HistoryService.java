package service.abs;

import model.events.Event;

import java.util.List;

public abstract class HistoryService {

    abstract public void putEvent(Event event);

    abstract public Event getEventById(int id);

    public abstract List<Event> getEventsByObject(int id);

    public abstract List<Event> getAllEvents();
}
