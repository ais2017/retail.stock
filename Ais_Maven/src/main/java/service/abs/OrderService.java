package service.abs;

import model.order.Order;

import java.util.List;

abstract public class OrderService {

    abstract public void putOrder(Order order);

    abstract public Order getOrderById(int id);

    public abstract List<Order> getOrdersByType(String type);

    public abstract List<Order> getAllOrders();
}
