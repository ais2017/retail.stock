package service.abs;

import model.building.Stoke;

import java.util.List;

public abstract class StokeService {

    abstract public void putStoke(Stoke stoke);

    abstract public Stoke getStokeById(int id);

    public abstract List<Stoke> getAllStoks();
}
