package service.abs;

import exceptionDir.service.UserNotFoundException;
import model.person.User;

public abstract class UserService {

    protected abstract User login(String username, String password) throws UserNotFoundException;

    public abstract boolean checkPassword(String username, String password);

    public abstract boolean isUserLogined(User user);

    public abstract void logout(User user);
}
