package model.building;

import exceptionDir.model.OverSizeException;
import model.goods.Food;
import model.goods.Thing;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class CellTest {

    @Test
    public void correctConstructorCellTest() {
        Cell cell = new Cell(0, true, 1, 2,3);
        assertEquals(0, cell.getCellId());
        assertEquals(true, cell.isCold());
        assertEquals(1, cell.getHeight());
        assertEquals(2,cell.getLength());
        assertEquals(3, cell.getWidth());
    }

    @Test
    public void testAdd() {
        Food badFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 60*60*1000));
        Cell cell = new Cell(0, true, 1, 2,3);
        try {
            cell.putGood(badFood);
        } catch (OverSizeException e) {
            e.printStackTrace();
        }
        assertEquals(1, cell.getGoods().size());
        assertEquals(1, badFood.getHeight());
        assertEquals(badFood, cell.getGoods().get(0));
        Thing goodThing = new Thing(1,1,1, 1, 1, "g", "income");
        try {
            cell.putGood(goodThing);
        } catch (OverSizeException e) {
            e.printStackTrace();
        }
        assertEquals(1, cell.getGoods().size());
        assertEquals(1, goodThing.getWidth());
        assertEquals(badFood, cell.getGoods().get(0));
    }

    @Test
    public void checkThrowableOverSize() {
        Food badFood = new Food(1,2,2,2,2,"kg", "income", new Date(), new Date(new Date().getTime() + 60*60*1000));
        Food goodFood = new Food(1,2,2,2,1,"kg", "income", new Date(), new Date(new Date().getTime() + 60*60*1000));
        Cell cell = new Cell(0, true, 1, 2,3);
        try {
            cell.putGood(badFood);
        } catch (OverSizeException e) {
            e.printStackTrace();
        }
        try {
            cell.putGood(goodFood);
        } catch (OverSizeException e) {
            e.printStackTrace();
            assertTrue(true);
            return;
        }
        assertTrue(false);
    }

}