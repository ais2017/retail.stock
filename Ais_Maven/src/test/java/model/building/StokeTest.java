package model.building;

import exceptionDir.model.OverCellException;
import model.goods.Food;
import model.goods.Good;
import model.goods.Partition;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static org.junit.Assert.*;

public class StokeTest {

    @Test
    public void correctConstructorStokeTest() {
        Stoke stoke = new Stoke(0, 1, 2);
        assertEquals(0, stoke.getBoxId());
        assertEquals(1, stoke.getxCoordinate());
        assertEquals(2, stoke.getyCoordinate());
    }

    @Test
    public void getBestGood() {
        Stoke stoke = new Stoke(0, 1, 2);
        stoke.setCells(new ArrayList<>());
        Date dateLate  = new Date();
        dateLate.setTime(new Date().getTime() + 24*60*60*1000);
        Food badFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 60*60*1000));
        Food goodFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 24*60*60*1000));
        Food goodFood2 = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 24*60*60*1000));
        Food goodFood3 = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 24*60*60*1000));
        stoke.getCells().add(new Cell(0, true,100,100,100));
        stoke.getCells().add(new Cell(0, true,100,100,100));
        stoke.getCells().get(0).getGoods().add(badFood);
        stoke.getCells().get(0).getGoods().add(goodFood);
        stoke.getCells().get(1).getGoods().add(goodFood2);
        stoke.getCells().get(1).getGoods().add(goodFood3);
        assertEquals(badFood, (Food)stoke.getBestGood("income"));
    }

    @Test
    public void findPlace() {
        Stoke stoke = new Stoke(0, 1, 2);
        stoke.setCells(new ArrayList<>());
        stoke.getCells().add(new Cell(0, true,100,100,100));
        stoke.getCells().add(new Cell(0, true,100,100,100));
    }

    @Test
    public void putCell() {
        Cell cell = new Cell(0, true, 1, 2,3);
        Stoke stoke = new Stoke(0, 1, 2);
        stoke.addCell(cell);
        assertEquals(1, stoke.getCells().size());
        assertEquals(1,cell.getHeight());
        assertEquals(cell,stoke.getCells().get(0));
    }

    @Test
    public void checkThrowableOverCell() {
        Cell cell = new Cell(0, true, 2, 2,2);
        Cell cell2 = new Cell(0, true, 1, 1,1);
        Stoke stoke = new Stoke(0, 1, 2);
        stoke.addCell(cell);
        stoke.addCell(cell2);
        Partition partition = new Partition(1, new HashMap<Good, Integer>());
        Food badFood = new Food(1,2,2,2,1,"kg", "income", new Date(), new Date(new Date().getTime() + 4*60*60*1000 - 1000));
        partition.getFoods().put(badFood,5);
        try {
            stoke.place(partition);
        } catch (OverCellException e) {
            assertTrue(true);
            return;
        }
        assertTrue(false);
    }
}