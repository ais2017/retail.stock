package model.goods;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class FoodTest {

    @Test
    public void constructorFoodTest() {
        Food badFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 60*60*1000));
        assertEquals(1, badFood.getWidth());
        assertEquals(1, badFood.getHeight());
        assertEquals(1, badFood.getLegth());
        assertEquals(1, badFood.getWeight());
        assertEquals(1, badFood.getCost());
        assertEquals("kg", badFood.getMetric());
        assertEquals("income", badFood.getStatus());
    }

    @Test
    public void getHoursLeft() {
        Food badFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 5*60*60*1000 - 1000));
        assertEquals(4, badFood.getHoursLeft());
    }
}