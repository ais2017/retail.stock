package model.goods;

import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class PartitionTest {

    @Test
    public void constructorPartitionTest() {
        Partition partition = new Partition(1, new HashMap<Good, Integer>());
        assertNotNull(partition.getFoods());
        assertEquals(1, partition.getIdPart());
    }

    @Test
    public void getMinShelfHours() {
        Partition partition = new Partition(1, new HashMap<Good, Integer>());
        Food badFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 4*60*60*1000 - 1000));
        Food goodFood = new Food(1,1,1,1,1,"kg", "income", new Date(), new Date(new Date().getTime() + 5*24*60*60*1000 - 1000));
        partition.getFoods().put(badFood,5);
        partition.getFoods().put(goodFood,6);
        assertEquals(3,partition.getMinShelfHours());
    }
}