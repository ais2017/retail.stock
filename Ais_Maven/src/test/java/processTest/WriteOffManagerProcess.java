package processTest;

import exceptionDir.service.UserNotFoundException;
import model.building.Stoke;
import model.goods.Food;
import model.goods.Good;
import model.goods.Partition;
import model.order.Order;
import model.order.ReturnAct;
import model.person.Manager;
import org.junit.Test;
import service.*;
import service.abs.DocumentService;
import service.abs.OrderService;
import service.abs.StokeService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class WriteOffManagerProcess {

    DocumentService documentService;
    OrderService orderService;
    StokeService stokeService;
    DummyUserService userService;
    DummyHistoryService dummyHistoryService;

    public WriteOffManagerProcess() {
        documentService = new DummyDocumentService();
        orderService = new DummyOrderService();
        stokeService = new DummyStokeService();
        userService = new DummyUserService();
        dummyHistoryService = new DummyHistoryService();
    }

    @Test
    public void writeOffManagerTest() {
        System.out.println("All Stoks");
        stokeService.getAllStoks().forEach(stoke -> System.out.println("Stoke " + stoke));
        System.out.println("All Orders in Start");
        this.orderService.getAllOrders().forEach(order1 -> System.out.println("Order " + order1));
        List<Stoke> stokes = stokeService.getAllStoks().stream().filter(stoke -> {
            List<Food> foods = stoke.isGoodDie();
            boolean bool = foods != null && stoke.isGoodDie().size() > 0;
            return bool;
        }).collect(Collectors.toList());
        System.out.println("Bad Stoks");
        stokes.forEach( stoke -> {
            System.out.println("Stoke " + stoke);
        });
        Manager manager = null;
        try {
            manager = (Manager)userService.login("user-manager","4");
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
        HashMap<Good,Integer> goods = new HashMap<Good, Integer>();
        Stoke stoke = stokes.get(0);
        for (Food food : stoke.isGoodDie()) {
            goods.put(food,1);
        }
        Order order3 = new ReturnAct(5, new Partition(0, goods), new Date(), "inProcess", false);
        manager.createReturnAct((ReturnAct)order3);
        System.out.println("All Orders in End");
        this.orderService.getAllOrders().forEach(order1 -> System.out.println("Order " + order1));
    }
}
