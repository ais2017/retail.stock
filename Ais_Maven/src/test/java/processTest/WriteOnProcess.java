package processTest;

import exceptionDir.model.OverCellException;
import model.building.Stoke;
import model.events.Event;
import model.order.Order;
import model.order.OrdinalOrder;
import model.order.ReturnAct;
import model.person.Storekeeper;
import org.junit.Test;
import service.*;
import service.abs.DocumentService;
import service.abs.OrderService;
import service.abs.StokeService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class WriteOnProcess {

    DocumentService documentService;
    OrderService orderService;
    StokeService stokeService;
    DummyUserService userService;
    DummyHistoryService dummyHistoryService;

    public WriteOnProcess() {
        documentService = new DummyDocumentService();
        orderService = new DummyOrderService();
        stokeService = new DummyStokeService();
        userService = new DummyUserService();
        dummyHistoryService = new DummyHistoryService();
    }

    @Test
    public void writeOnTest() {
        Storekeeper storekeeper = (Storekeeper)userService.getUser("storekeeper");
        List<Order> orders = this.orderService.getAllOrders();
        OrdinalOrder order = (OrdinalOrder)this.orderService.getOrderById(1);
        Stoke mystoke = stokeService.getStokeById(order.getPlaceTo().getBoxId());
        System.out.println("Stoke " + mystoke);

        mystoke.upload(order.getPartition());
        System.out.println("All Orders in Start");
        this.orderService.getAllOrders().forEach(order1 -> System.out.println("Order " + order1));
        System.out.println("Storekeeper " + storekeeper);
        Event event = new Event();
        event.setObjectId(storekeeper.getUserId());
        event.setMessage("StoreKeeper With Name " + storekeeper.getName() + " put order number" + order.getId() +
                " in time" + new Date().toString() + " in box " + mystoke.getBoxId());
        event.setEventId(1);
        Event event2 = new Event();
        event2.setObjectId(storekeeper.getBoxId());
        event2.setMessage("StoreKeeper With Name " + storekeeper.getName() + " put order number" + order.getId() +
                " in time" + new Date().toString() + " in box " + mystoke.getBoxId());
        event2.setEventId(2);
        dummyHistoryService.putEvent(event);
        dummyHistoryService.putEvent(event2);
        dummyHistoryService.getAllEvents().forEach( evento -> {
            System.out.println("Event " + evento);
        });
        order.setExec(true);
        orders = this.orderService.getOrdersByType("return").stream()
                .map( ordero -> (ReturnAct)ordero)
                .filter(ReturnAct::isCancel)
                .filter(returnAct -> !returnAct.isExec())
                .collect(Collectors.toList());
        System.out.println("Stoke " + mystoke);
        System.out.println("All Orders in Start");
        this.orderService.getAllOrders().forEach(order1 -> System.out.println("Order " + order1));
        System.out.println("All Needed finded Orders in End");
        orders.forEach( orderPrint -> {
            System.out.println("Order " + orderPrint);
        });
    }
}
